##  Introduction

This application aims to experiment with rendering a heatmap containing as much points as possible.

**Features:**
- Infinite zoom
- Pannable canvas at any zoom level
- Easily extendable

**Rendering "engines":**
- SVG using d3.js
- Native canvas rendering using the 2D context
- WebGL based rendering using Regl: https://github.com/regl-project/regl

**Optimisation techniques:**
- Lazy rendering on pan: use CSS translate on panning and render when mouse is released
- Using the crossfilter library: http://crossfilter.github.io/crossfilter/
- Pre-render canvas neighbour for better UX when panning

## npm commands

Before anything else, clone the repo and execute `npm install` in the root folder.

### npm run build
This command produces the distributable version of the GridPlan library. You'll find the minified version at `/dist/gridplan.min.js`.

### npm run devserver
It launches a development server with live reload functionality and automatically opens the example application in a new browser tab.