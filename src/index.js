import Vue from 'vue'
import vuewheel from 'vuewheel';
import App from '@/examples/madfast/MadfastDemo.vue'
// import App from '@/examples/randomdata/RandomDataApp.vue'
// import App from '@/examples/standalone/StandaloneApp.vue'

Vue.use(vuewheel)
new Vue({
  el         : '#app',
  components : { App },
  template   : '<App/>',
})
