import Vue from 'vue'
import vuewheel from 'vuewheel';
import App from './Heatmap.vue'

function createInstance(selector,config) {

	Vue.use(vuewheel)
	return new Vue({
		el         : selector,
		data       : {
			data    : config.data,
			options : config.options,
			plugins : config.plugins,
		},
		components : { App },
		template   : '<App/>',
	})
}

window['EXPERIMENTAL-MADFAST-GRIDPLAN'] = createInstance;

export default createInstance;

