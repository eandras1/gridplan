export function zoomCanvasToCoordinate(zoomTo,originalZoom,viewportOffset) {
    // magnifyingScale tells how many times should we multiply the current zoom level
    // In other words: it tells how much deeper do we want to go on the z axis
    let magnifyingScale  = 1/Math.pow(originalZoom.zoomFactor, zoomTo.delta/100);

    let currentHZoom = originalZoom.horizontalZoom;
    let currentVZoom = originalZoom.verticalZoom;
    let oldXOffset = viewportOffset.offsetX;
    let oldYOffset = viewportOffset.offsetY;

    let newXOffset = -(-oldXOffset*magnifyingScale + zoomTo.x*(magnifyingScale-1));
    let newYOffset = -(-oldYOffset*magnifyingScale + zoomTo.y*(magnifyingScale-1));
    let newHZoom = currentHZoom * magnifyingScale;
    let newVZoom = currentVZoom * magnifyingScale;
    let offsetDeltaX = newXOffset - viewportOffset.offsetX;
    let offsetDeltaY = newYOffset - viewportOffset.offsetY;

    return {
        magnifyingScale,
        newXOffset,
        newYOffset,
        newHZoom,
        newVZoom,
        offsetDeltaX,
        offsetDeltaY,
    }

}