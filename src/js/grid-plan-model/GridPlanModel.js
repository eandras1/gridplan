import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

import createLogger from 'vuex/dist/logger'
import state from './state'
import mutations from './mutations'
import getters from './getters'

const GridPlan = class {
    constructor(options) {
        // Mimic private class property by not declaring the store on "this"
        const vuexStore = new Vuex.Store({
            strict: true,
            state,
            mutations,
            getters,
            // plugins: [createLogger()],
        });

        this.getStore = () => vuexStore;
        this.eventListeners = {};
        this.setDefaults(options)
    }

    setDefaults(options) {
        if (options.viewPortWidth) {
            this.commitMutation('setViewPortWidth',options.viewPortWidth);
        }

        if (options.viewPortHeight) {
            this.commitMutation('setViewPortHeight',options.viewPortHeight);
        }

        if (options.rows && options.columns) {
            this.commitMutation('setGridSize',{
                x : options.columns,
                y : options.rows,
            });
        }

        if (options.cellSize) {
            this.commitMutation('setCellWidth',options.cellSize);
            this.commitMutation('setCellHeight',options.cellSize);
        }

        if (options.defaultOffset && options.defaultOffset) {
            this.commitMutation('setDefaultOffset',options.defaultOffset);
        }
    }

    commitMutation(mutationName,payload) {
        this.getStore().commit(mutationName,payload);
        this.notifyListeners('update');
    }

    zoomTo(zoomObj) {
        this.commitMutation('zoomTo',zoomObj);
        this.notifyListeners('zoom');
    }

    moveOffset(offset) {
        this.commitMutation('moveOffset',offset);
    }

    setOffset(offset) {
        this.commitMutation('setOffset',offset);
    }

    on(event,callback) {
        if (!(event in this.eventListeners)) {
            this.eventListeners[event] = [];
        }
        this.eventListeners[event].push(callback);
        // TODO return here a deregister function
    }

    notifyListeners(event) {
       if (event in this.eventListeners) {
            for (let callback of this.eventListeners[event]) {
                if (typeof callback === "function") {
                    callback(this.getStore().state)
                }
            }
       }
    }
}

/**
 * The proxy handler is responsible to redirect "get" calls within the GridPlan class
 * For more information visit: https://javascript.info/proxy
 */
const ProxyHandler = {
  get(gridplan, prop) {
    // TODO: we can have conflicting state and computed state names. Solve it.
    if (prop in gridplan.getStore().state) {
      return gridplan.getStore().state[prop];
    }
    else if (prop in gridplan.getStore().getters) {
      return gridplan.getStore().getters[prop];
    }
    else {
        return gridplan[prop];
    }
  }
};


/**
 * In a tradition setup Vuex states are computed states are accessible like this:
 *     - this.store.state.<propertyName>
 *     - this.store.getters.<computedProperty>
 *
 * To avoid the overhead of knowing where you can access which property,
 * these values are proxied and can be accessed directly:
 *    - this.<propertyName>
 *    - this.<computedProperty>
 *
 */
export default function(){
    return new Proxy(new GridPlan(...arguments), ProxyHandler)
} ;