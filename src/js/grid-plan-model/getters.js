/**
 * Getters are computed  properties based on state values
 * @type {Object}
 */
const Getters = {
	gridWidth(state) {
		return (state.grid.xCellCount * state.cell.width) * state.zoom.horizontalZoom + (state.grid.xCellCount-1) * state.grid.gutter;
	},

    gridHeight(state) {
		return (state.grid.yCellCount * state.cell.height) * state.zoom.verticalZoom + (state.grid.yCellCount-1) * state.grid.gutter;
    },

    gridSize (state,getters) {
    	return {
    		width: getters.gridWidth,
    		height: getters.gridHeight
    	}
    },

    fullOffsetX (state) {
        return state.viewport.defaultOffsetX + state.viewport.offsetX;
    },

    fullOffsetY (state) {
        return state.viewport.defaultOffsetY + state.viewport.offsetY;
    },

    cellWidth (state,getters) {
        return state.cell.width * state.zoom.horizontalZoom;
    },

    cellHeight (state,getters) {
        return state.cell.height * state.zoom.verticalZoom;
    },

    nrOfHorizontallyFittingCellsInViewport (state,getters) {
        return Math.floor(state.viewport.width/getters.cellWidth);
    },

    nrOfVerticallyFittingCellsInViewport (state,getters) {
        return Math.floor(state.viewport.height/getters.cellHeight);
    },

    // TODO: improve this algorithm
    visibleIndices(state,getters) {
        let xStart,xCount,yStart,yCount;

        let offsetX = getters.fullOffsetX;
        let offsetY = getters.fullOffsetY;

        // Calculate visible cells on the X axis
        let xOffsetCellCount = Math.ceil(offsetX/getters.cellWidth);
        let remainingSpace_AxisX = (offsetX<0) ? (state.viewport.width) : (state.viewport.width - offsetX);
        xStart = (offsetX < 0) ? Math.abs(xOffsetCellCount) : 0;
        xStart = Math.max(0,xStart);
        xStart = Math.min(xStart,state.grid.xCellCount-1);
        xCount = Math.ceil(remainingSpace_AxisX/getters.cellWidth)
        xCount = Math.max(xCount,0);
        xCount = Math.min(xCount,(state.grid.xCellCount-xStart));

        // Calculate visible cells on the Y axis
        let yOffsetCellCount = Math.ceil(offsetY/getters.cellHeight);
        let remainingSpace_AxisY = (offsetY<0) ? (state.viewport.height) : (state.viewport.height - offsetY);
        yStart = (offsetY < 0) ? Math.abs(yOffsetCellCount) : 0;
        yStart = Math.max(0,yStart);
        yStart = Math.min(yStart,state.grid.yCellCount-1);
        yCount = Math.ceil(remainingSpace_AxisY/getters.cellHeight)
        yCount = Math.max(yCount,0);
        yCount = Math.min(yCount,(state.grid.yCellCount-yStart));

        return { xStart, xCount, yStart, yCount }
    }
}

export default Getters;