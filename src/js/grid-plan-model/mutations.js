import Vue from 'vue';
import { zoomCanvasToCoordinate } from './GridPlanMathService'

export default {

    setGridSize: (state, payload) => {
        state.grid.xCellCount = payload.x;
        state.grid.yCellCount = payload.y;
    },

    setViewPortWidth: (state, width = 100) => {
        Vue.set(state.viewport, 'width', width)
    },

    setViewPortHeight: (state, height = 100) => {
        Vue.set(state.viewport, 'height', height)
    },

    setCellWidth: (state, width = 10) => {
        Vue.set(state.cell, 'width', width);
    },

    setCellHeight: (state, height = 10) => {
        Vue.set(state.cell, 'height', height);
    },

    setDefaultOffset: (state, defaultOffset) => {
        Vue.set(state.viewport, 'defaultOffsetX', defaultOffset.x);
        Vue.set(state.viewport, 'defaultOffsetY', defaultOffset.y);
    },

    setOffset: (state, offset) => {
        Vue.set(state.viewport, 'offsetX', offset.x);
        Vue.set(state.viewport, 'offsetY', offset.y);
    },

    moveOffset: (state, offset) => {
        Vue.set(state.viewport, 'offsetX', state.viewport.offsetX + offset.x);
        Vue.set(state.viewport, 'offsetY', state.viewport.offsetY + offset.y);
    },
    /**
     * zoom parameter must contains three attributes:
     *     x     - the x cordinate on the viewport where the zoom happened
     *     y     - the y cordinate on the viewport
     *     delta - is a double representing the vertical scroll amount in the WheelEvent.deltaMode unit
     */
    zoomTo: (state, zoomCoordinates) => {
        let zoomedState = zoomCanvasToCoordinate(zoomCoordinates, state.zoom, state.viewport);
    	let currentHZoom = state.zoom.horizontalZoom;
    	let currentVZoom = state.zoom.verticalZoom;

        // Don't allow too large zoom level
        // TODO: Define "too large"
        if (currentHZoom * zoomedState.magnifyingScale * state.cell.width > state.viewport.width/2) {
            return false;
        }

        // Don't allow a zoom level below 0.5px cell size
        if (currentHZoom * zoomedState.magnifyingScale * state.cell.width < 0.5) {
            return false;
        }

    	Vue.set(state.zoom, 'horizontalZoom', zoomedState.newHZoom);
    	Vue.set(state.zoom, 'verticalZoom', zoomedState.newVZoom);
        Vue.set(state.viewport, 'offsetX', zoomedState.newXOffset);
        Vue.set(state.viewport, 'offsetY', zoomedState.newYOffset);

        return 'valami';
    }
};