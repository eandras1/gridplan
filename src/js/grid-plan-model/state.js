const State = {

	/**
	 * Cells are the building blocks of a grid, they can be identified through their position in the grid
	 * Visually they are represented as rectangles (use 1:1 ratio for square representation)
	 */
	cell : {
		// Cell size horizontally
		width  : 10,
		// Cell size vertically
		height : 10,
	},

	/**
	 * The grid is a collection of cells in rows and columns
	 * The (0,0) position in the grid represents the cell in the top left corner
	 */
	grid : {
		// The number of cells in a row (xAxis)
		xCellCount : 10,
		// The number of cells in a column (yAxis)
		yCellCount : 10,
		// The distance between cells in the grid
		gutter     : 0,
	},

	/**
	 * Think of a viewport as a mask container of the grid,
	 * it shows only the visible part of the grid.
	 *
	 * The physical manifestation of the viewport can be a HTML <canvas> element, a PDF page or a PNG.
	 */
	viewport : {
		// Width of the viewport
		width  : 100,
		// Height of the viewport
		height : 100,
		// Horizontal distance of the (0,0) point of the grid from the (0,0) point of the viewport
		offsetX: 0,
		// Vertical distance of the grid from the viewport
		offsetY: 0,
		// By default draw every cell with an offset
		defaultOffsetX: 0,
		defaultOffsetY: 0,
	},

	/**
	 * The zoom level defines the magnifying level of the cells
	 */
	zoom : {
		// The multiplier of the cell's width value
		horizontalZoom : 1,
		// The multiplier of the cell's height value
		verticalZoom : 1,
		zoomFactor : 1.2
	}
}
export default State;