export default {

  updateMinimap : function (model) {
    // Options
    const minimapWidth = 200;
    const minimapRatio = minimapWidth / model.gridWidth;
    const minimapHeight = model.gridHeight * minimapRatio;

    let minimapEl = document.querySelector('.minimap');
    minimapEl.style.width = `${minimapWidth}px`;
    minimapEl.style.height = `${minimapHeight}px`;
    minimapEl.style.top = '20px';
    minimapEl.style.left = '20px';

    const visibleViewportWidth = model.viewport.width - 2* model.viewport.defaultOffsetX;
    const visibleViewportHeight = model.viewport.height - 2* model.viewport.defaultOffsetY;

    const rectangleWidth =  visibleViewportWidth * minimapRatio ;
    const rectangleHeight = visibleViewportHeight * minimapRatio;
    const deltaX = -model.viewport.offsetX * minimapRatio;
    const deltaY = -model.viewport.offsetY * minimapRatio;
    let rectangleEl = document.querySelector('.minimap-rectangle');
    rectangleEl.style.width = `${rectangleWidth}px`;
    rectangleEl.style.height = `${rectangleHeight}px`;
    rectangleEl.style.left = `${deltaX}px`;
    rectangleEl.style.top = `${deltaY}px`;
  },

  afterRender : function (model) {
    this.updateMinimap(model);
  },

  onPan: function(model) {
    this.updateMinimap(model);
  },

  onZoom: function(model) {
    this.updateMinimap(model);
  },

  onCreate: (model,heatmapElement) => {
    // Create canvas
    const minimap = document.createElement("div");
    minimap.className = "minimap"
    minimap.style.position = 'absolute';
    minimap.style.backgroundColor = 'rgba(0,0,0,0.4)';
    minimap.style.border = '1px solid rgba(0,0,0,0.2)';

    const innerWrapper = document.createElement("div");
    innerWrapper.className = 'minimap-inner-wrapper';
    innerWrapper.style.position = 'relative';
    innerWrapper.style.height = '100%';
    innerWrapper.style.width = '100%';
    innerWrapper.style.overflow = 'hidden';
    minimap.appendChild(innerWrapper);

    const rectangle = document.createElement("div");
    rectangle.className = 'minimap-rectangle'
    rectangle.style.position = 'absolute';
    rectangle.style.backgroundColor = 'rgba(255,255,255,0.3)';
    rectangle.style.border = '1px solid rgba(255,255,255,0.3)';
    innerWrapper.appendChild(rectangle);

    // Add to DOM
    heatmapElement.appendChild(minimap);
  },
  beforeRender: () => {},
  onPanEnd: () => {},
  onZoomEnd: () => {},
  onDestroy: () => {},
  onDataChange: () => {},
  onOptionChange: () => {},
}