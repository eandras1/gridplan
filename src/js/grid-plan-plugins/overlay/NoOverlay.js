export default function NoOverlay(model,cells,container){
	// Remove all nodes
	while (container.firstChild) {
		container.removeChild(container.firstChild);
	}
}