import * as d3 from 'd3'
export default function StructureOverlay(model,cells,container,vitamins){

	let d3Container = d3.select(container);
	const t = d3Container.transition().duration(400);

	const renderImages = (cell) => {
		let targetIndex = cell.index % vitamins.queryCount;
		let queryIndex = Math.floor(cell.index / vitamins.targetCount);

		return `
			<div style="padding: 20px;">
				<div style="margin-bottom: 10px;">
					<span style="display:inline-block; height:20px; width: 20px; background-color:${cell.hexColor}; position: relative; top: 3px;"></span>
					Dissimilarity: ${cell.value.toPrecision(3)}
				</div>
				<div style="font-size:11px; color: #666"><strong>Query:</strong> ${vitamins.queryIds[queryIndex]}</div>
				<img src="data:image/png;base64,${vitamins.targetMolBase64[targetIndex]}" width="50" height="50" alt=""/>
				<div style="font-size:11px; color: #666"><strong>Target:</strong> ${vitamins.targetIds[targetIndex]}</div>
				<img src="data:image/png;base64,${vitamins.queryMolBase64[queryIndex]}" width="50" height="50" alt=""/>
			</div>
		`;
	}


    d3Container.selectAll('div.cell')
		.data(cells,d => d.index)
		.join(
			enter => enter.append("div")
				.html(renderImages)
				.style("position",`absolute`)
				.style("border",`1px solid rgba(255,255,255,0.2)`)
				.call(enter => enter.transition(t).style("background-color",`#FFFFFF`))
			,update => update.
				style("display",`flex`)
				.style("border",`1px solid rgba(0,0,0,0.2)`)
				.call(enter => enter.transition(t).style("background-color",`#FFFFFF`))
				.html(renderImages)
		)
		.style("top", (cell) => `${cell.y}px`)
		.style("left", (cell) => `${cell.x}px`)
		.style("width", `${model.cellWidth}px`)
		.style("height",`${model.cellHeight}px`)
		.style("display",`block`)
		.style("justify-content",`center`)
		.style("align-items",`center`)
		.style("overflow",`hidden`)
		.style("font-family",`Helvetica, Arial`)
		.attr("class", "cell");
}