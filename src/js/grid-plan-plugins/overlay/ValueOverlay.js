import * as d3 from 'd3'

export default function ValueOverlay(model,cells,container){
	let d3Container = d3.select(container);
	const t = d3Container.transition().duration(400);

    d3Container.selectAll('div.cell')
		.data(cells,d => d.index)
		.join(
			enter => enter.append("div")
				.html((cell) => cell.value.toPrecision(3))
				.style("background-color",(cell) => `${cell.hexColor}`)
				.style("position",`absolute`)
				.style("border",`1px solid rgba(255,255,255,0.2)`)
				.style("opacity",`0`)
				.call(enter => enter.transition(t).style("opacity",`1`))
			,update => update
				.html((cell) => cell.value.toPrecision(3))
				.style("border",`1px solid rgba(255,255,255,0.2)`)
				.call(enter => enter.transition(t).style("background-color",(cell) => `${cell.hexColor}`))

		)
		.style("top", (cell) => `${cell.y}px`)
		.style("left", (cell) => `${cell.x}px`)
		.style("width", `${model.cellWidth}px`)
		.style("height",`${model.cellHeight}px`)
		.style("display",`flex`)
		.style("justify-content",`center`)
		.style("align-items",`center`)
		.style("overflow",`hidden`)
		.style("font-family",`Helvetica, Arial`)
		.attr("class", "cell");
}