import ValueOverlay from './ValueOverlay.js'
import StructureOverlay from './StructuresOverlay.js'
import NoOverlay from './NoOverlay.js'
import vitaminsRaw from '@/data/heatmap-vitamins-cfp7.txt'
const vitamins = JSON.parse(vitaminsRaw);

export default {

  afterRender : (model,cells) => {
    let container = document.querySelector('.cell-container');

    let lowerLimit = 70;
    let higherLimit = 200;

    if (model.cellWidth > lowerLimit && model.cellWidth < higherLimit) {
      return ValueOverlay(model,cells,container,vitamins)
    }
    else if ( model.cellWidth >= higherLimit) {
      return StructureOverlay(model,cells,container,vitamins)
    }
    else {
      return NoOverlay(model,cells,container,vitamins);
    }
  },

  onCreate: () => {},
  beforeRender: () => {},
  onPan: () => {},
  onPanEnd: () => {},
  onZoom: () => {},
  onZoomEnd: () => {},
  onDestroy: () => {},
  onDataChange: () => {},
  onOptionChange: () => {},
}