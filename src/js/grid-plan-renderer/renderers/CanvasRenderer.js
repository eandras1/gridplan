import GridPlanRenderer from './Renderer'

class CanvasRenderer extends GridPlanRenderer {

    constructor(container,model) {
    	super(container,model)
    }

    createCanvas() {
    	// Create canvas
    	this.canvas = document.createElement("canvas");
    	this.canvas.width = this.model.viewport.width;
    	this.canvas.height = this.model.viewport.height;
    	this.canvas.style.position = 'relative';
    	this.canvas.style.top = `-${this.model.viewport.defaultOffsetY}px`;
    	this.canvas.style.left = `-${this.model.viewport.defaultOffsetX}px`;

    	// Add to DOM
    	this.container.appendChild(this.canvas);

    	// Store 2D context
    	this.context = this.canvas.getContext('2d');
    }

    clearCanvas() {
  		this.context.clearRect(0, 0, this.model.viewport.width, this.model.viewport.height);
    }

    renderCell(cell) {
  		this.context.fillStyle = cell.hexColor || cell.color;
  		this.context.fillRect(cell.x, cell.y, this.model.cellWidth, this.model.cellHeight);
    }

    beforeRender() {
    	this.clearCanvas();
    	this.drawGrid();
    }

    drawGrid() {
      // For smaller cell size don't render the mesh
      if (this.model.cellWidth<10) {
        return;
      }
      this.context.lineWidth = 0.2;

      let moduloX = this.model.fullOffsetX % this.model.cellWidth;
      let moduloY = this.model.fullOffsetY % this.model.cellHeight;

      for (let i=0; i<=this.model.nrOfHorizontallyFittingCellsInViewport;i++) {
        this.context.beginPath();
        this.context.moveTo(i*this.model.cellWidth + moduloX,0);
        this.context.lineTo(i*this.model.cellWidth + moduloX , this.model.viewport.height);
        this.context.stroke();
      }
      for (let i=0; i<=this.model.nrOfVerticallyFittingCellsInViewport;i++) {
        this.context.beginPath();
        this.context.moveTo(0,i*this.model.cellHeight + moduloY);
        this.context.lineTo(this.model.viewport.width, i*this.model.cellHeight + moduloY);
        this.context.stroke();
      }
    }
}

export default CanvasRenderer;