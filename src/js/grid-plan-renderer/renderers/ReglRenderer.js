import GridPlanRenderer from './Renderer'
import regl from 'regl'

class ReglRenderer extends GridPlanRenderer {

    constructor(container,model) {
    	super(container,model);
    }

    createCanvas() {
      // Create canvas
      this.canvas = document.createElement("canvas");
      this.canvas.width = this.model.viewport.width;
      this.canvas.height = this.model.viewport.height;
      this.canvas.style.position = 'relative';
      this.canvas.style.top = `-${this.model.viewport.defaultOffsetY}px`;
      this.canvas.style.left = `-${this.model.viewport.defaultOffsetX}px`;

      // Add to DOM
      this.container.appendChild(this.canvas);

      // Create regl instance
      this.regl = regl(this.canvas);
    }

    clearCanvas() {
      this.regl.clear({
        color: [1, 1, 1, 1],
        depth: 1,
      });
    }

    beforeRender() {
      this.clearCanvas();
    }

    afterRender(rectangles) {
      // Convert all rgb(32,23,23) strings to an array of normalized colors (between 0-1)
      let color = rectangles.map(rect => this.normalizeRGB(rect.color));

      // Convert cell positions to vec2()
      // Translate all position by the size/2 of the cell, because the anchor point is
      // at the middle of the rectangle
      let position = rectangles.map(rect => [
        rect.x + this.model.cellWidth/2,
        rect.y + this.model.cellHeight/2
      ]);

      // TODO: optimise this cause drawRectangle is re-created every time a render has been called
      this.drawRectangle = this.regl({
        // Vertex Shader
        vert: `
          precision mediump float;
          attribute vec2 position;
          attribute vec3 color;

          // This will be sent to the fragment shader
          varying vec3 fragmentColor;

          // Uniform won't change through the rendering process
          uniform float cellWidth;
          uniform float cellHeight;
          uniform float canvasWidth;
          uniform float canvasHeight;

          // helper function to transform from pixel space to normalized
          // device coordinates (NDC). In NDC (0,0) is the middle,
          // (-1, 1) is the top left and (1, -1) is the bottom right.
          vec2 normalizeCoords(vec2 position) {
            return vec2(
              2.0 * ((position.x / canvasWidth) - 0.5),
              // invert y to treat [0,0] as bottom left in pixel space
              -(2.0 * ((position.y / canvasHeight) - 0.5)));
          }

          void main() {
            gl_PointSize = cellWidth;
            fragmentColor = color;
            gl_Position = vec4(normalizeCoords(position), 0.0, 1.0);
          }
        `,

        // Fragment shader
        frag: `
          precision mediump float;
          varying vec3 fragmentColor;

          void main() {
            gl_FragColor = vec4(fragmentColor, 1.0);
          }
        `,

        attributes: {
          position: position,
          color: color,
        },

        uniforms: {
          cellWidth    : this.regl.prop('cellWidth'),
          cellHeight   : this.regl.prop('cellHeight'),
          canvasWidth  : this.regl.prop('canvasWidth'),
          canvasHeight : this.regl.prop('canvasHeight'),
        },

        count: position.length,

        primitive: 'point',
      });

      this.drawRectangle({
        cellWidth    : this.model.cellWidth,
        cellHeight   : this.model.cellHeight,
        canvasWidth  : this.model.viewport.width,
        canvasHeight : this.model.viewport.height,
      });
    }

    normalizeRGB(rgbArray) {
      return rgbArray.map(color => parseInt(color)/255)
    }

    renderCell(cell) {
      // Do nothing in case of WebGL, because cells are not rendered one by one, but all at once
    }
}

export default ReglRenderer;