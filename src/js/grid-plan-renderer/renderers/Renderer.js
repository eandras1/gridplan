class GridPlanRenderer {
    constructor(container,model) {
    	this.container = container;
    	this.model = model;
        this.canvas = undefined;

    	this.createCanvas();
    }

    destroy() {
        this.container.removeChild(this.canvas);
    }

    createCanvas() { }

    clearCanvas() { }

    renderCell(cell) { }

    afterRender() { }

    beforeRender() {
        this.clearCanvas();
    }
}

export default GridPlanRenderer;