import GridPlanRenderer from './Renderer'
import * as d3 from 'd3'

class PixiRenderer extends GridPlanRenderer {

    constructor(container,model) {
    	super(container,model);
    }

    createCanvas() {
      // Create canvas
      this.canvas = document.createElement("div");
      this.canvas.id = "svg_heatmap";
      this.canvas.style.width = this.model.viewport.width + 'px';
      this.canvas.style.height = this.model.viewport.height + 'px';
      this.canvas.style.position = 'relative';
      this.canvas.style.top = `-${this.model.viewport.defaultOffsetY}px`;
      this.canvas.style.left = `-${this.model.viewport.defaultOffsetX}px`;

      // Add to DOM
      this.container.appendChild(this.canvas);
    }

    clearCanvas() {
		  document.getElementById('svg_heatmap').innerHTML = "";
    }

    beforeRender() {
      this.clearCanvas();

      this.svg = d3.select("#svg_heatmap")
        .append("svg")
        .attr("width", this.model.viewport.width)
        .attr("height", this.model.viewport.height)
        .append("g");
    }

    afterRender(data) {
      this.svg.selectAll()
        .data(data,d => d.index)
        .enter()
        .append("rect")
        .attr("x", (cell) => cell.x)
        .attr("y", (cell) => cell.y)
        .attr("width", this.model.cellWidth)
        .attr("height", this.model.cellHeight)
        .style("fill", (cell) => cell.hexColor)
    }

    renderCell(cell) {
    }
}

export default PixiRenderer;