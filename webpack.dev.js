const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
	// Loaders
	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules)/,
				use: [
					{
						loader: 'babel-loader?cacheDirectory=true',
						options: { presets: ['@babel/preset-env'] }
					}
				]
			},
			{
		        test: /\.vue$/,
		        loader: 'vue-loader',
		    },
		    {
			  test: /\.scss$/,
			  use: [
			    'vue-style-loader',
			    {
			      loader: 'css-loader',
			      options: { modules: false }
			    },
			    'sass-loader'
			  ]
			},
			{
				test: /\.(tpl|csv|txt)(\?.*)?$/,
				loader: 'raw-loader'
		    }
	    ]
	},
	entry   : {
		'app' : './src/index.js',
		'gridplan' : './src/js/grid-plan-heatmap/GridPlanHeatmap.js',
	},
	output: {
		filename : '[name].min.js',
		path     : path.resolve(__dirname, 'dist'),
		// publicPath: "/assets/",
	},
	resolve: {
		alias: {
	      '@': path.resolve(__dirname, 'src/'),
	      'vue$': 'vue/dist/vue.esm.js',
	    },
	},
	plugins: [
		// A webpack plugin to remove your build folder(s) before building
		new CleanWebpackPlugin(),
    	// Create the index.html for the main application
    	new HtmlWebpackPlugin({
	      // template: 'src/app.html',
	      filename: 'index.html',
	      title: 'GridPlan showcase',
	      template: 'src/index.html',
	    }),
	    // Force webpack-dev-server to write files to the output directory during development
	    new WriteFilePlugin(),
	    new VueLoaderPlugin()
	],
	optimization: {
		// splitChunks: {
		// 	chunks: 'all',
	 //    },
	    chunkIds: false
    },
    devServer: {
	    contentBase: './dist',
	    clientLogLevel: 'warning',
	    hot: true,
	    openPage: 'index.html',
	    publicPath: '/',
	    logLevel : 'debug',
	    overlay : true,
	    watchOptions: {
	      poll: false,
	    }
	},
	mode: 'development',
    devtool: 'inline-source-map',
};