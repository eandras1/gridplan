const merge = require('webpack-merge');
const common = require('./webpack.dev.js');

module.exports = merge(common, {
   mode: 'production',
   devtool: ''
   // Replace for source map generation 
   // devtool: 'inline-source-map'
});